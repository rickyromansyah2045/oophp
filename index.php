<?php 
require_once ('Animal.php');
require_once ('Frog.php');
require_once ('Ape.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br>" . "<br>"; // "no"


$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; 
echo $sungokong->yell(); // "Auooo"
echo "<br>" . "<br>";


$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // "shaun"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo $kodok->jump(); // "hop hop"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>